/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package th.ac.kku.coe.ws;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;

/**
 *
 * @author prat
 */

@WebServlet(urlPatterns = {"/GetGeoIpWs"})
public class GetGeoIpWs extends HttpServlet {
protected void processRequest(HttpServletRequest request,
	HttpServletResponse response)
	throws ServletException, IOException {
    	response.setContentType("text/xml;charset=UTF-8");
    	String ipAddr = request.getParameter("ip");
    	PrintWriter out = response.getWriter();
    	try {
        	MessageFactory mFac = MessageFactory.newInstance();
    
        	SOAPMessage message = mFac.createMessage();
           
        	SOAPBody body = message.getSOAPBody();
           
        	SOAPFactory soapFactory = SOAPFactory.newInstance();
      
      	  String prefix = "ns";
        	String namespace = "http://www.webservicex.net/";
        	Name opName = soapFactory.createName("GetGeoIP",
                      	prefix, namespace);
           
        	SOAPBodyElement opElem = body.addBodyElement(opName);
            
        	SOAPElement ip = opElem.addChildElement(
                    soapFactory.createName("IPAddress", prefix, namespace));
  	ip.addTextNode(ipAddr); 
            
        	MimeHeaders header = message.getMimeHeaders();
        	header.addHeader("SOAPAction", namespace + "GetGeoIP");
                
        	SOAPConnection soapConn =
                  	SOAPConnectionFactory.newInstance().createConnection();
        	String endpoint = "http://www.webservicex.net/geoipservice.asmx";
        	SOAPMessage resp = soapConn.call(message, endpoint);
                
                String targetIP = resp.getSOAPBody()
                    .getElementsByTagName("IP").item(0).getFirstChild().getNodeValue();
            String targetCountry = resp.getSOAPBody()
                    .getElementsByTagName("CountryName").item(0).getFirstChild().getNodeValue();
            out.println("<html>");
            out.println("<head>");
            out.println("<title>GetGeoIpWs2</title>");
            out.println("</head>");
            out.println("<body>");
            out.print("<font color=\"#5858FA\">"+targetIP+"</font>");
            out.println("<font> is in </font> "+"<font color=\"#5858FA\">"+targetCountry+"</font>");
            out.println("</body>");
            out.println("</html>");
          
        	
    	} catch (Exception ex) {
        	ex.printStackTrace();
    	} finally {
        	out.close();
    	}
}

	private void displayMessage(SOAPMessage message, PrintWriter out) {
    	try {
        	TransformerFactory tFac = TransformerFactory.newInstance();
        	Transformer transformer = tFac.newTransformer();
        	Source src = message.getSOAPPart().getContent();
        	StreamResult result = new StreamResult(out);
 	       transformer.transform(src, result);
    	} catch (Exception ex) {
        	ex.printStackTrace();
    	}
	}

   
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

