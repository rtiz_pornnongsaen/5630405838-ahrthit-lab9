package th.ac.kku.coe.ws;

import java.io.IOException;
import javax.xml.soap.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;

public class CallRDWS {

    public static void main(String[] args) throws IOException {
        try {
            MessageFactory mFac = MessageFactory.newInstance();

            SOAPMessage message = mFac.createMessage();

            SOAPBody body = message.getSOAPBody();

            SOAPFactory soapFactory = SOAPFactory.newInstance();

            String prefix = "ns";
            String namespace = "http://rdws.rd.go.th/serviceRD3/checktinpinservice";
            Name opName = soapFactory.createName("ServiceTIN",
                    prefix, namespace);
            SOAPBodyElement opElem = body.addBodyElement(opName);

            SOAPElement ip = opElem.addChildElement(
                    soapFactory.createName("username", prefix, namespace));
                    
          

            SOAPConnection soapConn
                    = SOAPConnectionFactory.newInstance().createConnection();
            String endpoint = "http://www.webservicex.net/geoipservice.asmx";
            SOAPMessage resp = soapConn.call(message, endpoint);

            //Display Request Message
            displayMessage(message);
            System.out.println("\n\n");
//add code below for trust x.509 ceritficate
            XTrustProvider.install();
            SOAPConnection conn
                    = SOAPConnectionFactory.newInstance().createConnection();
            SOAPMessage response = conn.call(message,
                    "https://rdws.rd.go.th/ServiceRD/CheckTINPINService.asmx");
            System.out.println("RESPONSE:");
//Display Response Message
            displayMessage(response);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void displayMessage(SOAPMessage message) throws Exception {
        TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer transformer = tFact.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        Source src = message.getSOAPPart().getContent();
        StreamResult result = new StreamResult(System.out);
        transformer.transform(src, result);
    }

}
